/*
 * Copyright 2015-2018 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */

package com.example.project;

public class FineCalculator {
	
	public int getFine(int kilometersTooFast) {
	    switch (kilometersTooFast) {
	        case 0: return 0;
	        case 10: return 10;
	        case 11: return 20;
	    }
	}
}
